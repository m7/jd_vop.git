<?php

namespace jd_vop\response\price;

use jd_vop\response\Result;

/**
 * 8.3  重新支付接口 Result
 */
class DoPay implements Result
{
    /**
     * @var
     */
    public $data;


    /**
     * 8.3  重新支付接口 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }
}