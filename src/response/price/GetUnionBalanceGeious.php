<?php

namespace jd_vop\response\price;

/**
 *  查询余额 Result Geious
 */
class GetUnionBalanceGeious
{

    /**
     * @var string 入参的pin值。
     */
    public $pin;
    /**
     * @var float 金采的违约金金额。
     */
    public $penaltySumAmt;
    /**
     * @var float 金采的总授信额度。
     */
    public $creditLimit;
    /**
     * @var float 金采的待还款额度。
     */
    public $debtSumAmt;
    /**
     * @var float 金采账户余额。
     */
    public $remainLimit;

    /**
     * @param string $pin 入参的pin值
     * @param float $penaltySumAmt 金采的违约金金额
     * @param float $creditLimit 金采的总授信额度
     * @param float $debtSumAmt 金采的待还款额度
     * @param float $remainLimit 金采账户余额。
     */
    public function __construct(string $pin, float $penaltySumAmt, float $creditLimit, float $debtSumAmt, float $remainLimit)
    {
        $this->pin = $pin;
        $this->penaltySumAmt = $penaltySumAmt;
        $this->creditLimit = $creditLimit;
        $this->debtSumAmt = $debtSumAmt;
        $this->remainLimit = $remainLimit;
    }

}