<?php

namespace jd_vop\response\price;

use jd_vop\response\Result;

/**
 *  8.2 查询余额变动明细 Result
 */
class GetBalanceDetail implements Result
{
    /**
     * @var int 记录总条数
     */
    public $total;
    /**
     * @var int 分页大小，默认20，最大1000
     */
    public $pageSize;
    /**
     * @var int 当前页码
     */
    public $pageNo;
    /**
     * @var int 总页数
     */
    public $pageCount;
    /**
     * @var array 业务数据 GetBalanceDetailData
     */
    public $data;

    /**
     * 8.2 查询余额变动明细 Result
     * @param int $total 记录总条数
     * @param int $pageSize 分页大小，默认20，最大1000
     * @param int $pageNo 当前页码
     * @param int $pageCount 总页数
     * @param array $data 业务数据 GetBalanceDetailData
     */
    public function __construct(int $total, int $pageSize, int $pageNo, int $pageCount, array $data)
    {
        $this->total = $total;
        $this->pageSize = $pageSize;
        $this->pageNo = $pageNo;
        $this->pageCount = $pageCount;
        $this->data = $data;
    }

    /**
     * @param $result
     * @return self
     */
    public static function parse($result): self
    {
        $total = $result['total'] ?? 0;
        $pageSize = $result['pageSize'] ?? 0;
        $pageNo = $result['pageNo'] ?? 0;
        $pageCount = $result['pageCount'] ?? 0;
        $data = [];
        foreach ($result['data'] ?? [] as $v) {
            $data[] = new GetBalanceDetailData(
                $v['id'] ?? 0,
                $v['accountType'] ?? 0,
                $v['amount'] ?? 0,
                $v['pin'] ?? "",
                $v['orderId'] ?? 0,
                $v['tradeType'] ?? 0,
                $v['tradeTypeName'] ?? "",
                $v['createdDate'] ?? "",
                $v['notePub'] ?? "",
                $v['tradeNo'] ?? 0
            );
        }
        return new self($total, $pageSize, $pageNo, $pageCount, $data);
    }
}