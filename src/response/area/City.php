<?php

namespace jd_vop\response\area;


use jd_vop\response\Result;

/**
 * 3.2 查询二级地址 Result
 */
class City implements Result
{
    /**
     * @var array 二级地址列表
     */
    public $cities;


    /**
     *  3.2 查询二级地址 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->cities = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}