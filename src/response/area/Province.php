<?php

namespace jd_vop\response\area;

use jd_vop\response\Result;

/**
 * 3.1 查询一级地址 Result
 */
class Province implements Result
{

    /**
     * @var array 一级地址数据
     */
    public $provinces;


    /**
     * 3.1 查询一级地址 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->provinces = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}