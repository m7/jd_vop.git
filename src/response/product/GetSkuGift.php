<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetSkuGift
 * @package jd_vop\response\product
 */
class GetSkuGift implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * GetSkuGift constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return GetSkuGift
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}