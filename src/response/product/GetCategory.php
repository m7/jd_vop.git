<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class GetCategory
 * @package jd_vop\response\product
 */
class GetCategory implements Result
{

    /**
     * @var integer 分类级别 0：一级分类；1：二级分类；2：三级分类；
     */
    public $catClass;
    /**
     * @var integer 分类ID
     */
    public $catId;
    /**
     * @var String 分类名称
     */
    public $name;
    /**
     * @var integer 父分类ID
     */
    public $parentId;
    /**
     * @var integer 状态 - 1：有效；0：无效
     */
    public $state;

    /**
     * 4.14 查询分类信息 Response
     * GetCategory constructor.
     * @param $catClass
     * @param $catId
     * @param $name
     * @param $parentId
     * @param $state
     */
    public function __construct($catClass, $catId, $name, $parentId, $state)
    {
        $this->catClass = $catClass;
        $this->catId = $catId;
        $this->name = $name;
        $this->parentId = $parentId;
        $this->state = $state;
    }

    /**
     * @param $result
     * @return GetCategory
     */
    public static function parse($result): self
    {
        $catClass = $result['catClass'] ?? 0;
        $catId = $result['catId'] ?? 0;
        $name = $result['name'] ?? [];
        $parentId = $result['parentId'] ?? [];
        $state = $result['state'] ?? [];
        return new self($catClass ,$catId ,$name ,$parentId ,$state);
    }

}