<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class TotalCheckNew
 * @package jd_vop\response\product
 */
class TotalCheckNew implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * TotalCheckNew constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return TotalCheckNew
     */
    public static function parse($result): self
    {
        return new self($result);
    }

}