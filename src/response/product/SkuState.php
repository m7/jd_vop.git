<?php

namespace jd_vop\response\product;

use jd_vop\response\Result;

/**
 * Class SkuState
 * @package jd_vop\response\product
 */
class SkuState implements Result
{
    /**
     * @var
     */
    public $data;

    /**
     * SkuState constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return SkuState
     */
    public static function parse($result): \jd_vop\response\product\SkuState
    {
        return new self($result);
    }

}