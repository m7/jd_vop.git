<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息 refundDetail
 */
class OrderRefundCompletedRefundDetail
{
    /**
     * @var int|mixed
     */
    public $id;
    /**
     * @var mixed|string  payid ⽀付单号
     */
    public $payid;
    /**
     * @var float|mixed refundAmount 退款⾦额；
     */
    public $refundAmount;
    /**
     * @var int|mixed refundJdBankId 退款平台编号（⽀付枚举值）
     */
    public $refundJdBankId;
    /**
     * @var mixed|string refundJdBankName 退款银⾏名称（⼯商银⾏，招商银⾏等）；
     */
    public $refundJdBankName;
    /**
     * @var int|mixed refundType 申请类型；
     */
    public $refundType;
    /**
     * @var int|mixed simpleCodeBankId 公司转账银⾏简码id；
     */
    public $simpleCodeBankId;

    /**
     * 11.1 查询推送信息 Result 119 申请单维度售后退款完成消息 refundDetail
     */
    public function __construct($result)
    {
        $this->id = $result[''] ?? 0;
        $this->payid = $result[''] ?? "";
        $this->refundAmount = $result[''] ?? 0;
        $this->refundJdBankId = $result[''] ?? 0;
        $this->refundJdBankName = $result[''] ?? "";
        $this->refundType = $result[''] ?? 0;
        $this->simpleCodeBankId = $result[''] ?? 0;
    }
}