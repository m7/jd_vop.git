<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 26 预定订单消息
 */
class AdvanceOrder
{

    /**
     * @var int 京东订单编号
     */
    public $orderId;


    /**
     * 11.1 查询推送信息 Result 26 预定订单消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
    }
}