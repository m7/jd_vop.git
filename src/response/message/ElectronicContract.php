<?php

namespace jd_vop\response\message;
/**
 * 11.1 查询推送信息 Result 117 电子合同消息
 */
class ElectronicContract
{

    /**
     * @var int 京东订单编号
     */
    public $jdOrderId;
    /**
     * @var string 京东账号
     */
    public $pin;
    /**
     * @var string
     */
    public $changeTime;
    /**
     * @var string
     * type1:生成；（确认订单后发出）
     * type2:更新；（退货类售后完成后发出）
     */
    public $type;
    /**
     * @var string
     */
    public $url;

    /**
     * 11.1 查询推送信息 Result 117 电子合同消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->jdOrderId = $result['jdOrderId'] ?? 0;
        $this->changeTime = $result['changeTime'] ?? "";
        $this->type = $result['type'] ?? "";
        $this->url = $result['url'] ?? "";
        $this->pin = $result['pin'] ?? "";
    }

}