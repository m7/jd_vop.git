<?php

namespace jd_vop\response\message;
/**
 * 11.1 查询推送信息 Result 16 商品信息变更
 * 包含：商品名称，介绍，规格参数和商品图变更
 */
class ProductInfoChange
{

    /**
     * @var int 商品编号
     */
    public $skuId;

    /**
     * 11.1 查询推送信息 Result 16 商品信息变更
     * 包含：商品名称，介绍，规格参数和商品图变更
     * @param $result
     */
    public function __construct($result)
    {
        $this->skuId = $result['skuId'] ?? 0;
    }
}