<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 23 订单配送退货消息
 */
class OrderReturn
{

    /**
     * @var int 京东订单编号
     */
    public $orderId;


    /**
     * 11.1 查询推送信息 Result 23 订单配送退货消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
    }
}