<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 102 专票资质审核进度消息
 * 此消息开通需要走线下邮件开通申请。
 */
class InvoiceExamineProcess
{

    /**
     * @var string
     */
    public $completeDate;
    /**
     * @var string
     */
    public $pushDate;
    /**
     * @var string
     */
    public $pins;
    /**
     * @var string
     */
    public $reason;
    /**
     * @var int
     */
    public $status;
    /**
     * @var string
     */
    public $unitName;
    /**
     * @var string
     */
    public $submitDate;
    /**
     * @var string
     */
    public $taxpayerId;
    /**
     * @var int
     */
    public $vatId;
    /**
     * 11.1 查询推送信息 Result 102 专票资质审核进度消息
     * 此消息开通需要走线下邮件开通申请。
     * @param $result
     */
    public function __construct($result)
    {
        $this->completeDate=$result['completeDate']??"";
        $this->pushDate=$result['pushDate']??"";
        $this->pins=$result['pins']??"";
        $this->reason=$result['reason']??"";
        $this->unitName=$result['unitName']??"";
        $this->submitDate=$result['submitDate']??"";
        $this->taxpayerId=$result['taxpayerId']??"";
        $this->status=$result['status']??0;
        $this->vatId=$result['vatId']??0;
    }
}