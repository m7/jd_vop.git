<?php

namespace jd_vop\response\message;
/**
 * 11.1 查询推送信息 Result 6 商品池内商品添加、删除消息
 */
class ProductPoolProductChange
{
    /**
     * @var int 商品编号
     */
    public $skuId;
    /**
     * @var int 商品池编号
     */
    public $page_num;
    /**
     * @var int 状态 1添加，2删除
     */
    public $state;

    /**
     * 11.1 查询推送信息 Result 6 商品池内商品添加、删除消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->state = $result['state'] ?? 0;
        $this->skuId = $result['skuId'] ?? 0;
        $this->page_num = $result['page_num'] ?? 0;
    }
}