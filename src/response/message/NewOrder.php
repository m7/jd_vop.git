<?php

namespace jd_vop\response\message;

/**
 * 11.1 查询推送信息 Result 25 新订单消息
 */
class NewOrder
{

    /**
     * @var int 京东订单编号
     */
    public $orderId;
    /**
     * @var string 京东账号
     */
    public $pin;


    /**
     * 11.1 查询推送信息 Result 25 新订单消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->orderId = $result['orderId'] ?? 0;
        $this->pin = $result['pin'] ?? "";
    }
}