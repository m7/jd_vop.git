<?php

namespace jd_vop\response\message;

/**
 *  11.1 查询推送信息 Result 108 混合支付微信个人支付成功消息
 */
class MixedPaymentWechatPaySuccess
{

    /**
     * @var int 京东订单编号
     */
    public $jdOrderId;
    /**
     * @var string 京东账号
     */
    public $pin;
    /**
     * @var string 三方支付单号
     */
    public $bizTradeNo;
    /**
     * @var float 支付金额
     */
    public $amount;


    /**
     * 11.1 查询推送信息 Result 108 混合支付微信个人支付成功消息
     * @param $result
     */
    public function __construct($result)
    {
        $this->jdOrderId = $result['jdOrderId'] ?? 0;
        $this->amount = $result['amount'] ?? 0;
        $this->bizTradeNo = $result['bizTradeNo'] ?? "";
        $this->pin = $result['pin'] ?? "";
    }
}