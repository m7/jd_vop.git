<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.3 提交订单 Result
 */
class SubmitOrder implements Result
{

    /**
     * @var int 京东订单号
     */
    public $jdOrderId;


    /**
     * @var float 运费（订单运费，需要记录此金额，否则可能会导致订单金额不一致），收取运费时返回
     */
    public $freight;

    /**
     * @var float 订单总金额（不含运费）
     */
    public $orderPrice;

    /**
     * @var float 订单未税金额（仅做页面展示，订单未税金额最终以发票票面为准。客户如需在下单后&开票前，获取尽量精准的订单未税金额，如用作以未税金额扣减预算等场景，可以联系VOP侧产品配置白名单）
     */
    public $orderNakedPrice;
    /**
     * @var float 订单税额
     */
    public $orderTaxPrice;

    /**
     * @var array 订单包含的商品信息列表
     */
    public $sku;

    /**
     * 7.3 提交订单 Result
     * @param int $jdOrderId 京东订单号
     * @param float $freight 运费（订单运费，需要记录此金额，否则可能会导致订单金额不一致），收取运费时返回
     * @param float $orderPrice 订单总金额（不含运费）
     * @param float $orderNakedPrice 订单未税金额（仅做页面展示，订单未税金额最终以发票票面为准。客户如需在下单后&开票前，获取尽量精准的订单未税金额，如用作以未税金额扣减预算等场景，可以联系VOP侧产品配置白名单）
     * @param float $orderTaxPrice 订单税额
     * @param array $sku 订单包含的商品信息列表
     */
    public function __construct(int $jdOrderId, float $freight, float $orderPrice,
                                float $orderNakedPrice, float $orderTaxPrice, array $sku)
    {
        $this->jdOrderId = $jdOrderId;
        $this->freight = $freight;
        $this->orderPrice = $orderPrice;
        $this->orderNakedPrice = $orderNakedPrice;
        $this->orderTaxPrice = $orderTaxPrice;
        $this->sku = $sku;
    }


    public static function parse($result): self
    {
        $jdOrderId = $result['jdOrderId'] ?? 0;
        $freight = $result['freight'] ?? 0;
        $orderPrice = $result['$orderPrice'] ?? 0;
        $orderNakedPrice = $result['orderNakedPrice'] ?? 0;
        $orderTaxPrice = $result['orderTaxPrice'] ?? 0;
        $skus = [];
        foreach ($result['sku'] ?? [] as $v) {
            $skus[] = new SubmitOrderBizSku($v);
        }
        return new self($jdOrderId, $freight, $orderPrice, $orderNakedPrice, $orderTaxPrice, $skus);
    }

}