<?php

namespace jd_vop\response\order;

/**
 * 7.3 提交订单 SubmitOrderBizSku
 */
class SubmitOrderBizSku
{
    /**
     * @var int 京东商品编号
     */
    public $skuId;
    /**
     * @var int 购买商品数量
     */
    public $num;
    /**
     * @var int 商品分类编号
     */
    public $category;
    /**
     * @var float 商品单价
     */
    public $price;
    /**
     * @var string 商品名称
     */
    public $name;
    /**
     * @var float 商品税率
     */
    public $tax;
    /**
     * @var float 商品税额
     */
    public $taxPrice;
    /**
     * @var float 商品未税价
     */
    public $nakedPrice;
    /**
     * @var int 商品类型：0普通、1附件、2赠品、3延保
     */
    public $type;
    /**
     * @var int 主商品skuid，如果本身是主商品，则oid为0
     */
    public $oid;

    /**
     * 7.3 提交订单 SubmitOrderBizSku
     * @param array $v
     */
    public function __construct(array $v)
    {
        $this->skuId = $v['skuId']??0;
        $this->num =  $v['num']??0;
        $this->category = $v['category']??0;
        $this->price =  $v['price']??0;
        $this->name =  $v['name']??'';
        $this->tax =  $v['tax']??0;
        $this->taxPrice =  $v['taxPrice']??0;
        $this->nakedPrice =  $v['nakedPrice']??0;
        $this->type =  $v['type']??0;
        $this->oid = $v['oid']??0;
    }


}