<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

/**
 * 7.16 批量确认收货接口 Result
 */
class BatchConfirmReceived
{
    /**
     * @var int 京东订单号
     */
    public $jdOrderId;
    /**
     * @var string 确认状态 1：成功，2：失败
     */
    public $confirmState;
    /**
     * @var string 失败描述，成功时为null
     */
    public $errorMsg;

    /**
     * 7.16 批量确认收货接口 Result
     * @param int $jdOrderId 京东订单号
     * @param string $confirmState 确认状态 1：成功，2：失败
     * @param string $errorMsg 失败描述，成功时为null
     */
    public function __construct(int $jdOrderId, string $confirmState, string $errorMsg)
    {
        $this->jdOrderId = $jdOrderId;
        $this->confirmState = $confirmState;
        $this->errorMsg = $errorMsg;
    }


}