<?php

namespace jd_vop\response\order;

use jd_vop\response\Result;

class SaveOrUpdatePoNo implements Result
{

    /**
     * @var
     */
    public $data;


    /**
     * 7.9 确认收货 Result
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param $result
     * @return static
     */
    public static function parse($result): self
    {
        return new self($result);
    }
}