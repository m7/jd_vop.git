<?php

namespace jd_vop\constant;

/**
 * 订单类型枚举
 */
interface  OrderType
{
    const DATA = [
        1 => "普通商品",
        2 => "大家电",
        3 => "实物礼品卡",
        4 => "售后换新单",
        5 => "厂家直送订单",
        6 => "FBP订单",
        7 => "生鲜",
        11 => "IBS订单",
        20 => "电子卡",
    ];
}