<?php

namespace jd_vop\request\message;

/**
 *  11.1 查询推送信息 Request GetType
 */
class GetType
{
    /**
     * @var int 订单拆分消息 1
     */
    public $orderSplit;
    /**
     * @var int 商品价格变更 2
     * 此消息数据量大且频繁，为防止消息大量积压，开通需要走线下邮件开通申请。
     */
    public $priceChange;
    /**
     * @var int 商品上下架变更消息 4
     */
    public $productStatusChange;
    /**
     * @var int 订单妥投消息 5
     */
    public $orderDelivery;
    /**
     * @var int 商品池内商品添加、删除消息 6
     */
    public $productPoolProductChange;
    /**
     * @var int 订单取消消息 10
     */
    public $orderCancel;
    /**
     * @var int 发票开票进度消息 11
     */
    public $invoicingProgress;
    /**
     * @var int 配送单生成成功消息 12
     */
    public $distributionOrderGeneration;
    /**
     * @var int 支付失败消息 14
     */
    public $payFail;
    /**
     * @var int 7天未支付取消消息/未确认取消消息 15
     */
    public $unconfirmedCancel;
    /**
     * @var int 商品信息变更 16
     */
    public $productInfoChange;
    /**
     * @var int 赠品促销变更消息 17
     */
    public $productGiftChange;
    /**
     * @var int 订单等待确认收货消息 18
     */
    public $orderWaitConfirm;
    /**
     * @var int 订单配送退货消息 23
     */
    public $orderReturn;
    /**
     * @var int 新订单消息 25
     */
    public $newOrder;
    /**
     * @var int 预定订单消息 26
     */
    public $advanceOrder;
    /**
     * @var int 旧售后服务单状态变更 28
     */
    public $serviceOrderChangeOld;
    /**
     * @var int 订单完成消息 31
     */
    public $orderCompletion;
    /**
     * @var int 商品池添加、删除消息 48
     */
    public $productPoolChange;
    /**
     * @var int 折扣率变更消息 49
     * 此消息开通需要走线下邮件开通申请。
     */
    public $discountRateChange;
    /**
     * @var int 京东地址变更消息 50
     * 此消息开通需要走线下邮件开通申请。
     */
    public $jdAddressChange;
    /**
     * @var int 商品税率变更消息（目前未涵盖全部商品） 100
     * 此消息开通需要走线下邮件开通申请
     */
    public $productTaxRateChange;
    /**
     * @var int 发票资质审核进度消息 102
     * 此消息开通需要走线下邮件开通申请
     */
    public $invoiceExamineProcess;
    /**
     * @var int 申请单环节变更消息 104
     */
    public $applicationFormLinkChange;
    /**
     * @var int 订单维度售后完成 105
     */
    public $orderAfterSalesCompletion;
    /**
     * @var int 混合支付微信个人支付成功消息 108
     */
    public $mixedPaymentWechatPaySuccess;
    /**
     * @var int 电子合同消息 117
     */
    public $electronicContract;
    /**
     * @var int 申请单维度售后退款完成消息 119
     */
    public $orderRefundCompleted;
    /**
     * @var int 商品可售状态变更 126
     */
    public $productSellStateChange;

    /**
     * 订单拆分消息
     */
    public function setOrderSplit(): void
    {
        $this->orderSplit = 1;
    }

    /**
     * 商品价格变更
     * 此消息数据量大且频繁，为防止消息大量积压，开通需要走线下邮件开通申请。
     */
    public function setPriceChange(): void
    {
        $this->priceChange = 2;
    }

    /**
     * 商品上下架变更消息
     */
    public function setProductStatusChange(): void
    {
        $this->productStatusChange = 4;
    }

    /**
     * 订单妥投消息
     */
    public function setOrderDelivery(): void
    {
        $this->orderDelivery = 5;
    }

    /**
     * 商品池内商品添加、删除消息
     */
    public function setProductPoolProductChange(): void
    {
        $this->productPoolProductChange = 6;
    }

    /**
     * 订单取消消息
     */
    public function setOrderCancel(): void
    {
        $this->orderCancel = 10;
    }

    /**
     * 发票开票进度消息
     */
    public function setInvoicingProgress(): void
    {
        $this->invoicingProgress = 11;
    }

    /**
     * 配送单生成成功消息
     */
    public function setDistributionOrderGeneration(): void
    {
        $this->distributionOrderGeneration = 12;
    }

    /**
     * 支付失败消息
     */
    public function setPayFail(): void
    {
        $this->payFail = 14;
    }

    /**
     * 7天未支付取消消息/未确认取消消息
     */
    public function setUnconfirmedCancel(): void
    {
        $this->unconfirmedCancel = 15;
    }

    /**
     * 商品信息变更
     */
    public function setProductInfoChange(): void
    {
        $this->productInfoChange = 16;
    }

    /**
     * 赠品促销变更消息
     */
    public function setProductGiftChange(): void
    {
        $this->productGiftChange = 17;
    }

    /**
     * 订单等待确认收货消息
     */
    public function setOrderWaitConfirm(): void
    {
        $this->orderWaitConfirm = 18;
    }

    /**
     * 订单配送退货消息
     */
    public function setOrderReturn(): void
    {
        $this->orderReturn = 23;
    }

    /**
     * 新订单消息
     */
    public function setNewOrder(): void
    {
        $this->newOrder = 25;
    }

    /**
     * 预定订单消息
     */
    public function setAdvanceOrder(): void
    {
        $this->advanceOrder = 26;
    }

    /**
     * 旧售后服务单状态变更
     */
    public function setServiceOrderChangeOld(): void
    {
        $this->serviceOrderChangeOld = 28;
    }

    /**
     * 订单完成消息
     */
    public function setOrderCompletion(): void
    {
        $this->orderCompletion = 31;
    }

    /**
     * 商品池添加、删除消息
     */
    public function setProductPoolChange(): void
    {
        $this->productPoolChange = 48;
    }

    /**
     * 折扣率变更消息
     * 此消息开通需要走线下邮件开通申请。
     */
    public function setDiscountRateChange(): void
    {
        $this->discountRateChange = 49;
    }

    /**
     * 京东地址变更消息
     * 此消息开通需要走线下邮件开通申请。
     */
    public function setJdAddressChange(): void
    {
        $this->jdAddressChange = 50;
    }

    /**
     * 商品税率变更消息（目前未涵盖全部商品）
     * 此消息开通需要走线下邮件开通申请。
     */
    public function setProductTaxRateChange(): void
    {
        $this->productTaxRateChange = 100;
    }

    /**
     * 专票资质审核进度消息
     * 此消息开通需要走线下邮件开通申请。
     */
    public function setInvoiceExamineProcess(): void
    {
        $this->invoiceExamineProcess = 102;
    }

    /**
     * 申请单环节变更消息
     */
    public function setApplicationFormLinkChange(): void
    {
        $this->applicationFormLinkChange = 104;
    }

    /**
     * 订单维度售后完成消息
     */
    public function setOrderAfterSalesCompletion(): void
    {
        $this->orderAfterSalesCompletion = 105;
    }

    /**
     * 混合支付微信个人支付成功消息
     */
    public function setMixedPaymentWechatPaySuccess(): void
    {
        $this->mixedPaymentWechatPaySuccess = 108;
    }

    /**
     * 电子合同消息
     */
    public function setElectronicContract(): void
    {
        $this->electronicContract = 117;
    }

    /**
     * 申请单维度售后退款完成消息
     */
    public function setOrderRefundCompleted(): void
    {
        $this->orderRefundCompleted = 119;
    }

    /**
     * 商品可售状态变更
     */
    public function setProductSellStateChange(): void
    {
        $this->productSellStateChange = 126;
    }

    /**
     * 11.1 查询推送信息 Request GetType
     * 查询全部
     */
    public function setAll($default = true): GetType
    {
        /**
         * 订单拆分消息
         */
        $this->setOrderSplit();
        /**
         * 商品价格变更
         * 此消息数据量大且频繁，为防止消息大量积压，开通需要走线下邮件开通申请。
         */
        if (!$default) {
            $this->setPriceChange();
        }
        /**
         * 商品上下架变更消息
         */
        $this->setProductStatusChange();
        /**
         * 订单妥投消息
         */
        $this->setOrderDelivery();
        /**
         * 商品池内商品添加、删除消息
         */
        $this->setProductPoolProductChange();
        /**
         * 订单取消消息
         */
        $this->setOrderCancel();
        /**
         * 发票开票进度消息
         */
        $this->setInvoicingProgress();
        /**
         * 配送单生成成功消息
         */
        $this->setDistributionOrderGeneration();
        /**
         * 支付失败消息
         */
        $this->setPayFail();
        /**
         * 7天未支付取消消息/未确认取消消息
         */
        $this->setUnconfirmedCancel();
        /**
         * 商品信息变更
         */
        $this->setProductInfoChange();
        /**
         * 赠品促销变更消息
         */
        $this->setProductGiftChange();
        /**
         * 订单等待确认收货消息
         */
        $this->setOrderWaitConfirm();
        /**
         * 订单配送退货消息
         */
        $this->setOrderReturn();
        /**
         * 新订单消息
         */
        $this->setNewOrder();
        /**
         * 预定订单消息
         */
        $this->setAdvanceOrder();
        /**
         * 旧售后服务单状态变更
         */
        $this->setServiceOrderChangeOld();
        /**
         * 订单完成消息
         */
        $this->setOrderCompletion();
        /**
         * 商品池添加、删除消息
         */
        $this->setProductPoolChange();
        /**
         * 折扣率变更消息
         * 此消息开通需要走线下邮件开通申请。
         */
        if (!$default) {
            $this->setDiscountRateChange();
        }
        /**
         * 京东地址变更消息
         * 此消息开通需要走线下邮件开通申请。
         */
        if (!$default) {
            $this->setJdAddressChange();
        }
        /**
         * 商品税率变更消息（目前未涵盖全部商品）
         * 此消息开通需要走线下邮件开通申请。
         */
        if (!$default) {
            $this->setProductTaxRateChange();
        }
        /**
         * 专票资质审核进度消息
         * 此消息开通需要走线下邮件开通申请。
         */
        if (!$default) {
            $this->setInvoiceExamineProcess();
        }
        /**
         * 申请单环节变更消息
         */
        $this->setApplicationFormLinkChange();
        /**
         * 订单维度售后完成消息
         */
        $this->setOrderAfterSalesCompletion();
        /**
         * 混合支付微信个人支付成功消息
         */
        $this->setMixedPaymentWechatPaySuccess();
        /**
         * 电子合同消息
         */
        $this->setElectronicContract();
        /**
         * 申请单维度售后退款完成消息
         */
        $this->setOrderRefundCompleted();
        /**
         * 商品可售状态变更
         */
        $this->setProductSellStateChange();
        return $this;
    }

    public function parse(): string
    {
        $setters = [];
        foreach ($this as $v) {
            if ($v > 0) {
                $setters[] = $v;
            }
        }
        return implode(",", $setters);
    }
}