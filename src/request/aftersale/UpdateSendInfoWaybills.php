<?php

namespace jd_vop\request\aftersale;


/**
 * 9.4 填写运单信息 waybill
 */
class UpdateSendInfoWaybills
{
    /**
     * @var array UpdateSendInfoWaybills 数组
     */
    public $list;


    /**
     * 9.4 填写运单信息 waybill
     */
    public function __construct()
    {
        $this->list = [];
    }

    /**
     * 添加运单信息
     * @param string $expressCompany 发货公司
     * @param string $expressCode 货运单号，字段长度不超过50
     * @param string $deliverDate 发货日期，格式为yyyy-MM-dd HH:mm:ss
     * @param float $freightMoney 运费
     * @param string $wareId 商品编号
     * @param int $wareType 商品类型。10主商品，20赠品。
     * @param int $wareNum 商品数量
     * @return bool
     */
    public function add(string $expressCompany, string $expressCode, string $deliverDate, float  $BigDecimal, string $wareId, int $wareType, int $wareNum): bool
    {
        $flag = false;
        foreach ($this->list as $v) {
            if ($v instanceof UpdateSendInfoWaybill) {
                if ($v->getExpressCode() == $expressCode) {
                    $flag = true;
                    break;
                }
            }
        }
        if (!$flag) {
            if (count($this->list) < 50) {
                $this->list[] = new UpdateSendInfoWaybill($expressCompany, $expressCode, $deliverDate, $BigDecimal,$wareId,$wareType,$wareNum);
                $flag = true;
            }
        }
        return $flag;
    }

    /**
     * 解析数据
     * @return false|string
     */
    public function parse()
    {
        return $this->list;
    }
}