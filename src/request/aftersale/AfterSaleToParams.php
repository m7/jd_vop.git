<?php

namespace jd_vop\request\aftersale;

/**
 * 9.1 商品售后 参数处理
 * Class AfterSaleToParams
 * @package jd_vop\request\aftersale
 */
class AfterSaleToParams
{

    /**
     * @var array
     */
    protected $paramsArr;

    /**
     * AfterSaleToParams constructor.
     * @param $wareIds string 商品编码，多个用英文逗号隔开
     */
    public function __construct(string $paramstring)
    {
        $this->paramsArr = explode(',',$paramstring);
    }

    /**
     * @return array
     */
    public function parse()
    {
        return $this->paramsArr;
    }

}