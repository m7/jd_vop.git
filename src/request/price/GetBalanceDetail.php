<?php

namespace jd_vop\request\price;

use jd_vop\request\Request;

/**
 * 8.2 查询余额变动明细 Request
 */
class GetBalanceDetail extends Request
{
    /**
     * @var string
     */
    protected static $uri = "api/price/getBalanceDetail";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 分页查询当前页数，默认为1
     */
    public $pageNum;
    /**
     * @var int 每页记录数，默认为20
     */
    public $pageSize;
    /**
     * @var string 订单号或流水单
     */
    public $orderId;
    /**
     * @var string 开始日期，格式必须：yyyyMMdd
     */
    public $startDate;
    /**
     * @var string  截止日期，格式必须：yyyyMMdd
     */
    public $endDate;

    /**
     * 8.2 查询余额变动明细  Request
     * @param string $token access token
     * @param mixed $pageNum 分页查询当前页数，默认为1
     * @param mixed $pageSize 每页记录数，默认为20
     * @param mixed $orderId 订单号或流水单
     * @param mixed $startDate 开始日期，格式必须：yyyyMMdd
     * @param mixed $endDate 截止日期，格式必须：yyyyMMdd
     */
    public function __construct(string $token, int $pageNum, $pageSize, $orderId, $startDate, $endDate)
    {
        parent::__construct();
        $this->token = $token;
        $this->pageNum = $pageNum;
        $this->pageSize = $pageSize;
        $this->orderId = $orderId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        $data = [
            'token' => $this->token,
            'pageNum' => $this->pageNum,
            'pageSize' => $this->pageSize,
            'orderId' => $this->orderId,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
        ];
        if (is_null($data['pageNum'])) {
            unset($data['pageNum']);
        }
        if (is_null($data['pageSize'])) {
            unset($data['pageSize']);
        }
        if (is_null($data['orderId'])) {
            unset($data['orderId']);
        }
        if (is_null($data['startDate'])) {
            unset($data['startDate']);
        }
        if (is_null($data['endDate'])) {
            unset($data['endDate']);
        }
        return $data;
    }
}