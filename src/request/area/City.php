<?php

namespace jd_vop\request\area;

use jd_vop\request\Request;

/**
 * 3.2 查询二级地址 Request
 */
class City extends Request
{

    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 一级地址ID
     */
    public $province_id;
    /**
     * @var string
     */
    protected static $uri = "api/area/getCity";

    /**
     * 3.2 查询二级地址 Request
     * @param $token string access token
     * @param $province_id  int 一级地址ID
     */
    public function __construct(string $token, int $province_id)
    {
        parent::__construct();
        $this->token = $token;
        $this->province_id = $province_id;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'id' => $this->province_id,
        ];
    }


}