<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class TotalCheckNew
 * @package jd_vop\request\product
 */
class TotalCheckNew extends Request
{
    /**
     * @var String 授权时获取的access token
     */
    protected $token;
    /**
     * @var String 商品编号
     */
    protected $skuIds;
    /**
     * @var String 一级地址编号
     */
    protected $province;
    /**
     * @var String 二级地址编号
     */
    protected $city;
    /**
     * @var String 三级地址编号
     */
    protected $county;
    /**
     * @var String 四级地址编号（有则必传，没有默认是 0 ）
     */
    protected $town;

    /**
     * @var string
     */
    protected static $uri = "api/product/totalCheckNew";

    /**
     * 4.15 商品可采校验接口 Request
     * TotalCheckNew constructor.
     * @param $token String 授权时获取的access token
     * @param $province String 一级地址编号
     * @param $city String 二级地址编号
     * @param $county String 三级地址编号
     * @param $town String 四级地址编号（有则必传，没有默认是 0 ）
     * @param $skuIds String 商品编号
     */
    public function __construct($token, $province, $city, $county, $town, $skuIds)
    {
        parent::__construct();

        $this->token    = $token;
        $this->skuIds   = $skuIds;
        $this->province = $province;
        $this->city     = $city;
        $this->county   = $county;
        $this->town     = $town ?? 0;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token'     => $this->token,
            'skuIds'    => $this->skuIds,
            'province'  => $this->province,
            'city'      => $this->city,
            'county'    => $this->county,
            'town'      => $this->town,
        ];
    }

}