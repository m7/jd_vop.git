<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

class GetBatchIsCod extends Request
{
    /**
     * @var String 授权时获取的access token
     */
    protected $token;
    /**
     * @var String 商品编号
     */
    protected $skuIds;
    /**
     * @var String 一级地址编号
     */
    protected $province;
    /**
     * @var String 二级地址编号
     */
    protected $city;
    /**
     * @var String 三级地址编号
     */
    protected $county;
    /**
     * @var String 四级地址编号（有则必传，没有默认是 0 ）
     */
    protected $town;

    /**
     * @var string
     */
    protected static $uri = "api/product/getBatchIsCod";

    protected $queryExts;

    /**
     * @param $token string 授权token
     * @param $skuIds string 商品编号，支持批量，以’,’分隔  (最高支持100个商品)
     * @param $province string 一级分类
     * @param $city string 二级地址编号
     * @param $county string 三级地址编号
     * @param $town string 四级地址编号(如果该地区有四级地址，则必须传递四级地址，没有四级地址则传0)
     * @param $queryExts GetIsCodQueryExt
     */
    public function __construct(string $token, string $skuIds, string $province, string $city, string $county, string $town, GetBatchIsCodQueryExt $queryExts)
    {
        parent::__construct();

        $this->token    = $token;
        $this->skuIds   = $skuIds;
        $this->province = $province;
        $this->city     = $city;
        $this->county   = $county;
        $this->town     = $town ?? 0;
        $this->queryExts = $queryExts;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token'     => $this->token,
            'skuId'    => $this->skuIds,
            'province'  => $this->province,
            'city'      => $this->city,
            'county'    => $this->county,
            'town'      => $this->town,
            'queryExts' => $this->queryExts->parse(),
        ];
    }

}