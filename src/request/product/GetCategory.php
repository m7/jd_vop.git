<?php

namespace jd_vop\request\product;

use jd_vop\request\Request;

/**
 * Class GetCategory
 * @package jd_vop\request\product
 */
class GetCategory extends Request
{
    /**
     * @var string 授权token
     */
    protected $token;
    /**
     * @var string 分类id
     */
    protected $cid;
    /**
     * @var string
     */
    protected static $uri = "api/product/getCategory";

    /**
     * 4.14 查询分类信息 Request
     * GetCategory constructor.
     * @param $token string 授权token
     * @param $cid string 分类id
     */
    public function __construct($token, $cid)
    {
        parent::__construct();
        $this->token = $token;
        $this->cid = $cid;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'cid' => $this->cid
        ];
    }

}