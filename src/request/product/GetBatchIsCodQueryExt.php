<?php

namespace jd_vop\request\product;

/**
 * Class GetBatchIsCodQueryExt
 * @package jd_vop\request\product
 */
class GetBatchIsCodQueryExt
{
    /**
     * @var
     */
    public $skuIds;           //返回具体的skuId明细

    /**
     * 商品编码
     */
    public function setSkuIds()
    {
        $this->skuIds = 1;
    }

    /**
     * @return string
     */
    public function parse(): string
    {
        $setters = [];
        foreach ($this as $k => $v) {
            if ($v == 1) {
                $setters[] = $k;
            }
        }
        return implode(",", $setters);
    }

}