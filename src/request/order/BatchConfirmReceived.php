<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.16 批量确认收货接口 Request
 */
class BatchConfirmReceived extends Request
{
    protected static $uri = "api/order/batchConfirmReceived";
    /**
     * @var  string $token access token
     */
    public $token;
    /**
     * @var string $jdOrderIds 京东子单号，请以，(英文逗号)分割。
     * 例如：129408,129409
     * (最高支持50个订单)
     */
    public $jdOrderIds;

    /**
     * 7.16 批量确认收货接口 Request
     * @param string $token access token
     * @param string $jdOrderIds 京东子单号，请以，(英文逗号)分割。
     * 例如：129408,129409
     * (最高支持50个订单)
     */
    public function __construct(string $token, string $jdOrderIds)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderIds = $jdOrderIds;

    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderIds' => $this->jdOrderIds,
        ];
    }


}