<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.5 确认预占库存订单 Request
 */
class ConfirmOrder extends Request
{
    protected static $uri = "api/order/confirmOrder";

    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 京东的订单单号(下单返回的父订单号)
     */
    public $jdOrderId;
    /**
     * @var string 采购单号
     */
    public $poN0;

    /**
     * 7.5 确认预占库存订单 Request
     * @param string $token access token
     * @param int $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @param string $poN0 采购单号
     */
    public function __construct(string $token, int $jdOrderId, string $poN0)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
        $this->poN0 = $poN0;
    }

    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
            'poN0' => $this->poN0,
        ];
    }

}