<?php

namespace jd_vop\request\order;

/**
 * 7.3 提交订单 sku
 */
class SubmitOrderSku
{
    /**
     * @var int 商品编号
     */
    public $skuId;
    /**
     * @var int 购买数量
     */
    public $num;
    /**
     * @var double 商品价格
     * 该字段不传值将查询京东最新的售卖价下单；
     * 传值时会校验传入价格和当前价格是否相等：如果价格不相等，下单失败。
     */
    public $price;
    /**
     * @var bool 是否需要增品，默认不给增品，默认值为：false，如果需要增品bNeedGift请给true
     */
    public $bNeedGift;
    /**
     * @var array  延保商品信息
     */
    public $yanbao;
    /**
     * @var array  对于有sku维度扩展字段需求的用户，提交订单时可以定义扩展字段信息，key需要提前申请开通
     */
    public $customSkuExt;

    /**
     * 7.3 提交订单 sku
     * @param $skuId int 商品编号
     * @param $num int 购买数量
     * @param float $price 商品价格
     * 该字段不传值将查询京东最新的售卖价下单；
     * 传值时会校验传入价格和当前价格是否相等：如果价格不相等，下单失败。
     */
    public function __construct(int $skuId, int $num, float $price = 0)
    {
        $this->skuId = $skuId;
        $this->num = $num;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getSkuId(): int
    {
        return $this->skuId;
    }

    /**
     * @param int $skuId
     */
    public function setSkuId(int $skuId): void
    {
        $this->skuId = $skuId;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isBNeedGift(): bool
    {
        return $this->bNeedGift;
    }

    /**
     * @param bool $bNeedGift
     */
    public function setBNeedGift(bool $bNeedGift): void
    {
        $this->bNeedGift = $bNeedGift;
    }

    /**
     * @return array
     */
    public function getYanbao(): array
    {
        return $this->yanbao;
    }

    /**
     * @param array $yanbao
     */
    public function setYanbao(array $yanbao): void
    {
        $this->yanbao = $yanbao;
    }

    /**
     * @return array
     */
    public function getCustomSkuExt(): array
    {
        return $this->customSkuExt;
    }

    /**
     * @return int
     */
    public function getNum(): int
    {
        return $this->num;
    }

    /**
     * @param array $customSkuExt
     */
    public function setCustomSkuExt(array $customSkuExt): void
    {
        $this->customSkuExt = $customSkuExt;
    }

    /**
     * @param int $num
     */
    public function setNum(int $num)
    {
        $this->num = $num;
        if ($this->num < 0) {
            $this->num = 0;
        }
    }


}