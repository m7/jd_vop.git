<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.8 查询配送信息 Request
 */
class OrderTrack extends Request
{

    /**
     * @var string
     */
    protected static $uri = "api/order/orderTrack";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var string 京东的订单单号(下单返回的父订单号)
     */
    public $jdOrderId;
    /**
     * @var int  是否返回订单的配送信息。0不返回配送信息。1，返回配送信息。
     * 只支持最近2个月的配送信息查询。
     */
    public $waybillCode;

    /**
     * 7.8 查询配送信息 Request
     * @param string $token  access token
     * @param string $jdOrderId  京东的订单单号(下单返回的父订单号)
     * @param int $waybillCode 是否返回订单的配送信息
     */
    public function __construct(string $token, string $jdOrderId, int $waybillCode)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
        $this->waybillCode = $waybillCode;
    }


    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
            'waybillCode' => $this->waybillCode
        ];
    }

}