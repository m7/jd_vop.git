<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.4 反查订单
 */
class SelectJdOrderIdByThirdOrder extends Request
{

    /**
     * @var string
     */
    protected static $uri = "api/order/selectJdOrderIdByThirdOrder";
    /**
     * @var string access token
     */
    public $token;
    /**
     * @var  string 第三方订单号（非京东订单号）。
     */
    public $thirdOrder;

    /**
     * @param string $token access token
     * @param string $thirdOrder 第三方订单号（非京东订单号）。
     */
    public function __construct(string $token, string $thirdOrder)
    {
        parent::__construct();
        $this->token = $token;
        $this->thirdOrder = $thirdOrder;
    }

    public function params(): array
    {
        return [
            "token" => $this->token,
            "thirdOrder" => $this->thirdOrder,
        ];
    }


}