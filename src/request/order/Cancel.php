<?php

namespace jd_vop\request\order;

use jd_vop\request\Request;

/**
 * 7.6 取消未确认订单 Request
 */
class Cancel extends Request
{

    protected static $uri = "api/order/cancel";

    /**
     * @var string access token
     */
    public $token;
    /**
     * @var int 京东的订单单号(父订单号)
     */
    public $jdOrderId;

    /**
     * 7.6 取消未确认订单 Request
     * @param $token string access token
     * @param $jdOrderId int 京东的订单单号(父订单号)
     */
    public function __construct(string $token, int $jdOrderId)
    {
        parent::__construct();
        $this->token = $token;
        $this->jdOrderId = $jdOrderId;
    }

    /**
     * @return array
     */
    public function params(): array
    {
        return [
            'token' => $this->token,
            'jdOrderId' => $this->jdOrderId,
        ];
    }

}