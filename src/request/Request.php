<?php

namespace jd_vop\request;

use GuzzleHttp\Client;
use jd_vop\constant\Constant;
use jd_vop\exception\NetWorkException;
use Psr\Http\Message\ResponseInterface;

abstract class Request implements RequestImpl
{
    protected $client;

    protected static $uri;

    /**
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => Constant::BASE_URI,
            'timeout' => Constant::REQUEST_TIME_OUT,
        ]);
    }

    /**
     *
     * @return array
     */
    public  function params(): array
    {
        return [];
    }

    /**
     * @return ResponseInterface
     * @throws NetWorkException
     */
    public function doRequest(): ResponseInterface
    {
//        var_dump($this::$uri,$this->params());
        try {
            $response = $this->client->post($this::$uri, [
                'headers' => [
                    'Content-Type' => "application/x-www-form-urlencoded"
                ],
                'form_params' => $this->params()
            ]);
        } catch (\Exception $e) {
            throw new NetWorkException();
        }
        return $response;

    }
}