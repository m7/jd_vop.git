<?php

namespace jd_vop;

use jd_vop\request\order\CustomExt;
use jd_vop\request\order\GetFreightSkus;
use jd_vop\request\order\SelectJdOrderQueryExt;
use jd_vop\response\order\BatchConfirmReceived;
use jd_vop\response\order\BatchConfirmReceivedList;
use jd_vop\response\order\Cancel;
use jd_vop\response\order\CheckCompleteOrder;
use jd_vop\response\order\CheckDlokOrder;
use jd_vop\response\order\CheckNewOrder;
use jd_vop\response\order\CheckRefuseOrder;
use jd_vop\response\order\ConfirmOrder;
use jd_vop\response\order\ConfirmReceived;
use jd_vop\response\order\GetFreight;
use jd_vop\response\order\GetPromiseTips;
use jd_vop\response\order\OrderTrack;
use jd_vop\response\order\SaveOrUpdatePoNo;
use jd_vop\response\order\SelectJdOrder;
use jd_vop\response\order\SelectJdOrderIdByThirdOrder;
use jd_vop\response\order\SubmitOrder;

class Order extends Jd_vop
{
    /**
     * 7.1 查询运费
     * @param $token string access token
     * @param GetFreightSkus $skus sku数组
     * @param $province int 一级地址id
     * @param $city int 二级地址id
     * @param $county int 三级地址id
     * @param $town int 四级地址id
     * @param $paymentType  int 京东支付方式 constant\PaymentType
     * @param $queryExts string 扩展查询字段,  conFreight //续重运费  todo 修改为扩展类库
     * @throws exception\NetWorkException
     * @throws exception\BizException
     * @throws exception\DataException
     */
    public function GetFreight(string $token, GetFreightSkus $skus, int $province,
                               int    $city, int $county, int $town, int $paymentType,
                               string $queryExts)
    {
        $request = new request\order\GetFreight($token, $skus, $province, $city, $county, $town, $paymentType, $queryExts);
        $response = $this->Req($request, GetFreight::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.2 查询预约日历
     * @param $token String 授权时获取的access token
     * @param $province String 一级地址编号
     * @param $city String 二级地址编号
     * @param $county String 三级地址编号
     * @param $town String 四级地址编号（有则必传，没有默认是 0 ）
     * @param $paymentType Int 支付类型
     * @param GetFreightSkus $skuIds String 商品编号
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function PromiseCalendar($token, $province, $city, $county, $town, $paymentType, GetFreightSkus $skuIds){
        $request = new request\order\PromiseCalendar($token, $province, $city, $county, $town, $paymentType, $skuIds);
        $response = $this->Req($request, \jd_vop\response\order\PromiseCalendar::class);
        return $response->result;
    }


    /**
     * 7.3 提交订单
     * @param request\order\SubmitOrder $request 7.3 提交订单 request
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function SubmitOrder(request\order\SubmitOrder $request)
    {
        $response = $this->Req($request, SubmitOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.4 反查订单
     * @param $token string Access token
     * @param $thirdOrder string 第三方订单号（非京东订单号）。
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function SelectJdOrderIdByThirdOrder(string $token, string $thirdOrder)
    {
        $request = new request\order\SelectJdOrderIdByThirdOrder($token, $thirdOrder);
        $response = $this->Req($request, SelectJdOrderIdByThirdOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.5 确认预占库存订单
     * @param string $token access token
     * @param int $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @param string $poN0 采购单号
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function ConfirmOrder(string $token, int $jdOrderId, string $poN0 = "")
    {
        $request = new request\order\ConfirmOrder($token, $jdOrderId, $poN0);
        $response = $this->Req($request, ConfirmOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.6 取消未确认订单
     * @param string $token access token
     * @param int $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function Cancel(string $token, int $jdOrderId)
    {
        $request = new request\order\Cancel($token, $jdOrderId);
        $response = $this->Req($request, Cancel::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.7 查询订单详情
     * @param string $token access token
     * @param int $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @param SelectJdOrderQueryExt $queryExts 扩展参数
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function SelectJdOrder(string $token, int $jdOrderId, SelectJdOrderQueryExt $queryExts)
    {
        $request = new request\order\SelectJdOrder($token, $jdOrderId, $queryExts);
        $response = $this->Req($request, SelectJdOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.8 查询配送信息
     * @param string $token access token
     * @param string $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @param int $waybillCodets 是否返回订单的配送信息
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function OrderTrack(string $token, string $jdOrderId, int $waybillCodets)
    {
        $request = new request\order\OrderTrack($token, $jdOrderId, $waybillCodets);
        $response = $this->Req($request, OrderTrack::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.9 确认收货
     * @param string $token access token
     * @param string $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function ConfirmReceived(string $token, string $jdOrderId)
    {
        $request = new request\order\ConfirmReceived($token, $jdOrderId);
        $response = $this->Req($request, ConfirmReceived::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.10 更新采购单号
     * @param string $token access token
     * @param string $jdOrderId 京东的订单单号(下单返回的父订单号)
     * @param string $poNo 采购单号，长度范围[1-26]
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function SaveOrUpdatePoNo(string $token, string $jdOrderId, string $poNo)
    {
        $request = new request\order\SaveOrUpdatePoNo($token, $jdOrderId, $poNo);
        $response = $this->Req($request, SaveOrUpdatePoNo::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.11 更新订单扩展字段
     * @param $token
     * @param $jdOrderId  订单号,更新扩展字段时以订单号作为唯一区分，拆单需要传子单号
     * @param CustomOrderExt $customOrderExt 对于有sku维度扩展字段需求的用户，提交订单后可以更新扩展字段信息，key需要提前申请开通
     * @param CustomSkuExt $customSkuExt 对于有sku维度扩展字段需求的用户，提交订单后可以更新扩展字段信息，key需要提前申请开通
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function UpdateCustomOrderExt($token, $jdOrderId, CustomExt $customOrderExt, CustomExt $customSkuExt){
        $request = new request\order\UpdateCustomOrderExt($token, $jdOrderId, $customOrderExt, $customSkuExt);
        $response = $this->Req($request, \jd_vop\response\order\UpdateCustomOrderExt::class);
        return $response->result;
    }


    /**
     * 7.12 查询新建订单列表
     * @param string $token access token
     * @param string $date 查询日期
     * @param mixed $pageNo 页码
     * @param mixed $pageSize 页大小取值范围
     * @param mixed $jdOrderIdIndex 最小订单号索引游标
     * @param mixed $endDate 结束日期
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function CheckNewOrder(string $token, string $date,  $pageNo,
                                      $pageSize, $jdOrderIdIndex, $endDate)
    {
        $request = new request\order\CheckNewOrder($token, $date, $pageNo, $pageSize,
            $jdOrderIdIndex, $endDate);
        $response = $this->Req($request, CheckNewOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.13 查询妥投订单列表
     * @param string $token access token
     * @param string $date 查询日期
     * @param mixed $pageNo 页码
     * @param mixed $pageSize 页大小取值范围
     * @param mixed $jdOrderIdIndex 最小订单号索引游标
     * @param mixed $endDate 结束日期
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function CheckDlokOrder(string $token, string $date,  $pageNo,
                                       $pageSize, $jdOrderIdIndex, $endDate)
    {
        $request = new request\order\CheckDlokOrder($token, $date, $pageNo, $pageSize,
            $jdOrderIdIndex, $endDate);
        $response = $this->Req($request, CheckDlokOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.14 查询拒收订单列表
     * @param string $token access token
     * @param string $date 查询日期
     * @param mixed $pageNo 页码
     * @param mixed $pageSize 页大小取值范围
     * @param mixed $jdOrderIdIndex 最小订单号索引游标
     * @param mixed $endDate 结束日期
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function CheckRefuseOrder(string $token, string $date,  $pageNo,
                                         $pageSize, $jdOrderIdIndex, $endDate)
    {
        $request = new request\order\CheckRefuseOrder($token, $date, $pageNo, $pageSize,
            $jdOrderIdIndex, $endDate);
        $response = $this->Req($request, CheckRefuseOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.15 查询完成订单列表
     * @param string $token access token
     * @param string $date 查询日期
     * @param mixed $pageNo 页码
     * @param mixed $pageSize 页大小取值范围
     * @param mixed $jdOrderIdIndex 最小订单号索引游标
     * @param mixed $endDate 结束日期
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function CheckCompleteOrder(string $token, string $date,  $pageNo,
                                           $pageSize, $jdOrderIdIndex, $endDate)
    {
        $request = new request\order\CheckCompleteOrder($token, $date, $pageNo, $pageSize,
            $jdOrderIdIndex, $endDate);

        $response = $this->Req($request, CheckCompleteOrder::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.16 查询配送预计送达时间
     * @param $token string access token
     * @param string $skuId 商品编号
     * @param string $num 数量
     * @param $province int 一级地址id
     * @param $city int 二级地址id
     * @param $county int 三级地址id
     * @param $town int 四级地址id
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function GetPromiseTips(string $token, string $skuId, string $num, int $province,
                                   int    $city, int $county, int $town)
    {
        $request = new request\order\GetPromiseTips($token, $skuId, $num, $province,
            $city, $county, $town);

        $response = $this->Req($request, GetPromiseTips::class);
        // 4. 成功数据
        return $response->result;
    }

    /**
     * 7.16 批量确认收货接口
     * @param $token string access token
     * @param string $jdOrderIds 京东子单号，请以，(英文逗号)分割。
     * 例如：129408,129409
     * (最高支持50个订单)
     * @return mixed
     * @throws exception\BizException
     * @throws exception\DataException
     * @throws exception\NetWorkException
     */
    public function BatchConfirmReceived(string $token, string $jdOrderIds)
    {
        $request = new request\order\BatchConfirmReceived($token, $jdOrderIds);

        $response = $this->Req($request, BatchConfirmReceivedList::class);
        // 4. 成功数据
        return $response->result;
    }

}