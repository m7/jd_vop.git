<?php

namespace jd_vop\tests;

use jd_vop\Product;
use jd_vop\request\product\GetBatchIsCodQueryExt;
use jd_vop\request\product\GetDetailQueryExt;
use jd_vop\request\product\GetIsCodQueryExt;
use jd_vop\request\product\ProCheckQueryExt;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;

/**
 * Class ProductTest
 * @package jd_vop\tests
 */
class ProductTest extends TestCase
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->product = new Product($client_id, $username, $password_md5, $client_secret);


    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetPageNum()
    {
        $result = $this->product->GetPageNum(Env::get("access_token"));
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testQuerySkuByPage()
    {
        $result = $this->product->QuerySkuByPage(Env::get("access_token"), "268776037", 10, 0);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetDetail()
    {
        $QueryExt = new GetDetailQueryExt();
//        $QueryExt->setCapacity();
        $QueryExt->setNappintroduction();
        $result = $this->product->GetDetail(Env::get("access_token"), "100007807293", $QueryExt);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testSkuImage()
    {
        $result = $this->product->SkuImage(Env::get("access_token"), "114942");
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testSkuState()
    {
        $result = $this->product->SkuState(Env::get("access_token"), "114942,114943");
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testProCheck()
    {
        $queryExt = new ProCheckQueryExt();
        $result = $this->product->ProCheck(Env::get("access_token"), "114942,114943", $queryExt);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testCheckAreaLimit(){
        $result = $this->product->CheckAreaLimit(Env::get("access_token"), "114942,114943",'1','1','1','');
        $this->assertNotEmpty($result);
        var_dump($result);
    }


    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetSkuGift(){
        $result = $this->product->GetSkuGift(Env::get("access_token"), "100015570724",'1','1','1','0');
        $this->assertNotEmpty($result);
        var_dump($result);
    }


    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetYanBaoSku(){
        $result = $this->product->GetYanBaoSku(Env::get("access_token"), "100015570724",'1','1','1','');
        $this->assertNotEmpty($result);
        var_dump($result);
    }


    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetIsCod(){
        $queryExt = new GetIsCodQueryExt();
        $result = $this->product->GetIsCod(Env::get("access_token"), "100015570724",'1','1','1','', $queryExt);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetBatchIsCod(){
        $queryExt = new GetBatchIsCodQueryExt();
        $result = $this->product->GetBatchIsCod(Env::get("access_token"), "100015570724",'1','1','1','', $queryExt);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGoodsSearch(){
        $result = $this->product->GoodsSearch(Env::get("access_token"), "衣服", '', 1 ,5 ,'' ,'' ,'' ,'' ,'' ,"price_asc" ,'' ,'' ,null);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetSimilarSku(){
        $result = $this->product->GetSimilarSku(Env::get("access_token"), "100015570724");
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetCategory(){
        //652;828;842
        $result = $this->product->GetCategory(Env::get("access_token"), "652");
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testTotalCheckNew(){
        $result = $this->product->TotalCheckNew(Env::get("access_token"), "1",'1','1','0','100015570724');
        $this->assertNotEmpty($result);
        var_dump($result);
    }




    //商品结束//

}