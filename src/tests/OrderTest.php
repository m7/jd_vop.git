<?php

namespace jd_vop\tests;

use jd_vop\constant\PaymentType;
use jd_vop\Order;
use jd_vop\request\order\CustomExt;
use jd_vop\request\order\CustomOrderExt;
use jd_vop\request\order\CustomSkuExt;
use jd_vop\request\order\GetFreightSkus;
use jd_vop\request\order\SelectJdOrderQueryExt;
use jd_vop\request\order\SubmitOrder;
use jd_vop\request\order\SubmitOrderSkus;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;

class OrderTest extends TestCase
{
    protected $order;

    /**
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->order = new Order($client_id, $username, $password_md5, $client_secret);


    }

    /**
     *  7.1 查询运费
     */
    public function testGetFreight()
    {
        $skus = new GetFreightSkus();
        $skus->add(7321529, 3);
        $skus->subtract(7321529, 2);
        $result = $this->order->GetFreight(Env::get("access_token"), $skus, 5, 258, 41497, 41868, PaymentType::CHARGE, "conFreight");
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 7.2 检查预约日历
    */
    public function testPromiseCalendar()
    {
        $sku = new GetFreightSkus();
        $sku->add(7321529, 3);
        $result = $this->order->PromiseCalendar(Env::get("access_token"), 1,2,3,4,5,$sku);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 7.3 提交订单
     */
    public function testSubmitOrder()
    {
        $skus = new SubmitOrderSkus();
        $skus->add(7321529, 1, 16.50);
        $thirdOrder = "test" . rand(100000, 999999);
        $request = new SubmitOrder(
            Env::get("access_token"),
            $thirdOrder,
            $skus,
            Env::get("consignee_name"),
            5, 258, 41497, 41868,
            Env::get("consignee_address"),
            Env::get("consignee_mobile"),
            2, 3, 5,
            1, PaymentType::CHARGE, 1,
            0, Env::get("consignee_mobile"), Env::get("regCompanyName"), Env::get("regCode")
        );
        $request->companyName = Env::get("company_name");
        $result = $this->order->SubmitOrder($request);
        var_dump($thirdOrder);
        var_dump($result);
        $this->assertNotEmpty($result);
        // jdOrderId 234256570145
    }

    /**
     * 7.4 反查订单
     */
    public function testSelectJdOrderIdByThirdOrder()
    {

        $result = $this->order->SelectJdOrderIdByThirdOrder(Env::get("access_token"),
            "test671814");
        var_dump($result);
        $this->assertNotEmpty($result);
    }


    /**
     * 7.5 确认预占库存订单
     */
    public function testConfirmOrder()
    {
        $result = $this->order->ConfirmOrder(Env::get("access_token"), "234473500394");
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.6 取消未确认订单
     */
    public function testCancel()
    {
        $result = $this->order->Cancel(Env::get("access_token"), "234256570145");
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.7 查询订单详情
     */
    public function testSelectJdOrder()
    {
        $querExt = new SelectJdOrderQueryExt();
        $result = $this->order->SelectJdOrder(Env::get("access_token"),
            "234473500394", $querExt);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.8 查询配送信息
     */
    public function testOrderTrack()
    {
        $result = $this->order->OrderTrack(Env::get("access_token"),
            "234473500394", 1);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.9 确认收货
     */
    public function testConfirmReceived()
    {
        $result = $this->order->ConfirmReceived(Env::get("access_token"),
            "234473500394");
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.10 更新采购单号
     */
    public function testSaveOrUpdatePoNo()
    {
        $result = $this->order->SaveOrUpdatePoNo(Env::get("access_token"),
            "234473500394", "111111");
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.11 更新订单扩展字段
    */
    public function testUpdateCustomOrderExt(){
        $customExtOrder = new CustomExt();
        $customExtSku = new CustomExt();

        $customExtOrder->orderAdd('skuId','5555');
        $customExtOrder->orderAdd('num','6666');
        $customExtSku->skuAdd('skuId','8888','8888');
        $customExtSku->skuAdd('num','8899988','8888');
        $customExtSku->skuAdd('skuId','99999','999');

        $result = $this->order->UpdateCustomOrderExt(Env::get("access_token"), "234473500394", $customExtOrder,$customExtSku);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.12 查询新建订单列表
     */
    public function testCheckNewOrder()
    {
        $date = date("Y-m-d");
        $result = $this->order->CheckNewOrder(Env::get("access_token"), $date, 1,
            100, null, null);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.13 查询妥投订单列表
     */
    public function testCheckDlokOrder()
    {
        $date = date("Y-m-d");
        $result = $this->order->CheckDlokOrder(Env::get("access_token"), $date, 1,
            100, null, null);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     * 7.14 查询拒收订单列表
     */
    public function testCheckRefuseOrder()
    {
        $date = date("Y-m-d");
        $result = $this->order->CheckRefuseOrder(Env::get("access_token"), $date, 1,
            100, null, null);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     *  7.15 查询完成订单列表
     */
    public function testCheckCompleteOrder()
    {
        $date = date("Y-m-d");
        $result = $this->order->CheckCompleteOrder(Env::get("access_token"), $date, 1,
            100, null, null);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     *  7.16 查询配送预计送达时间
     */
    public function testGetPromiseTips()
    {
        $result = $this->order->GetPromiseTips(Env::get("access_token"), 7321529, 1, 5, 258, 41497, 41868);
        var_dump($result);
        $this->assertNotEmpty($result);
    }

    /**
     *  7.16 批量确认收货接口
     */
    public function testBatchConfirmReceived()
    {
        $result = $this->order->BatchConfirmReceived(Env::get("access_token"), "234473500394,234473500394");
        var_dump($result);
        $this->assertNotEmpty($result);
    }


}