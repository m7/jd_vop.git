<?php

namespace jd_vop\tests;

use jd_vop\Price;
use jd_vop\request\price\GetSellPriceQueryExt;
use MillionMile\GetEnv\Env;
use PHPUnit\Framework\TestCase;

/**
 * Class ProductTest
 * @package jd_vop\tests
 */
class PriceTest extends TestCase
{
    /**
     * @var
     */
    protected $price;

    /**
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $client_id = Env::get('client_id');
        $username = Env::get('username');
        $password_md5 = strtolower(md5(Env::get('password')));
        $client_secret = Env::get('client_secret');
        $this->price = new Price($client_id, $username, $password_md5, $client_secret);


    }

    /**
     * @throws \jd_vop\exception\BizException
     * @throws \jd_vop\exception\DataException
     * @throws \jd_vop\exception\NetWorkException
     */
    public function testGetSellPrice()
    {
        $queryExt = new GetSellPriceQueryExt();
        $result = $this->price->GetSellPrice(Env::get("access_token"),
            "100007807293", json_encode([['skuId'=>"100007807293","num"=>1]]), $queryExt);
        $this->assertNotEmpty($result);
        var_dump($result);
    }


    /**
     * 8.1 查询余额
     */
    public function testGetUnionBalance()
    {
        $result = $this->price->GetUnionBalance(Env::get("access_token"),
            Env::get("username"), "1,2");
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 8.2 查询余额变动明细
     */
    public function testGetBalanceDetail()
    {
        $result = $this->price->GetBalanceDetail(Env::get("access_token"),
            1, 20, null, null, null);
        $this->assertNotEmpty($result);
        var_dump($result);
    }

    /**
     * 8.3  重新支付接口
     */
    public function testDoPay()
    {
        $result = $this->price->DoPay(Env::get("access_token"),
            "234408385806");
        $this->assertNotEmpty($result);
        var_dump($result);
    }


    //价格接口结束//

}